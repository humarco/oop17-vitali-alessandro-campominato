package interfaces;

/**
 * interfaccia di DialogEndGame
 * 
 * @author Alessandro
 *
 */
public interface DialogEndGameInterface {
	
  /**
   * 
   * @return il nome da attribuire al record
   */
	public String getStringName();

}
